const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./src/**/*.{vue,js}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      white: colors.white,
      black: colors.black,
      gray: colors.gray,
      beige: {
        100: "#E0DBD0",
        500: "#F6F4EF",
      },
      green: "#8DCC7B"
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
